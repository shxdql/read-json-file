﻿using UnityEngine;
using UnityEngine.UI;

public class BubbleChat : MonoBehaviour
{
    [SerializeField] TextAsset json;

    [SerializeField] Text nameText, emailText;

    [SerializeField] int index;

    DataList myData;

    [System.Serializable]
    public class Data
    {
        public string id;
        public string name;
        public Image avatar;
        public string email;
        public string mobileNumber;
        public string password;
    }

    [System.Serializable]
    public class DataList
    {
        public Data[] data;
    }

    private void Start()
    {
        myData = new DataList();

        myData = JsonUtility.FromJson<DataList>(json.text);
    }

    private void Update()
    {
        nameText.text = myData.data[index].name;
        emailText.text = myData.data[index].email;
    }
}
