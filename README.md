# Read JSON File

# 4210181017 Muhammad Shodiqul Ihsan

# Source Code

```
using UnityEngine;
using UnityEngine.UI;
public class BubbleChat : MonoBehaviour
{
    [SerializeField] TextAsset json;

    [SerializeField] Text nameText, emailText;

    [SerializeField] int index;

    DataList myData;

    [System.Serializable]
    public class Data
    {
        public string id;
        public string name;
        public Image avatar;
        public string email;
        public string mobileNumber;
        public string password;
    }

    [System.Serializable]
    public class DataList
    {
        public Data[] data;
    }

    private void Start()
    {
        myData = new DataList();

        myData = JsonUtility.FromJson<DataList>(json.text);
    }

    private void Update()
    {
        nameText.text = myData.data[index].name;
        emailText.text = myData.data[index].email;
    }
}
```

# Output

![](https://media.giphy.com/media/TXP9lxp2C1vPl0JsKd/giphy.gif)

# Description

```
[SerializeField] TextAsset json;

[SerializeField] Text nameText, emailText;

[SerializeField] int index;
```
Declare JSON file, UI Text for showing the name and email of the data in JSON File, and index for ID of the data that we want to show.

```
DataList myData;

[System.Serializable]
public class Data
{
    public string id;
    public string name;
    public Image avatar;
    public string email;
    public string mobileNumber;
    public string password;
}

[System.Serializable]
public class DataList
{
    public Data[] data;
}
```
Create a custom class for load the data. The variable must be the same as in the JSON File.

```
private void Start()
{
    myData = new DataList();

    myData = JsonUtility.FromJson<DataList>(json.text);
}
```
Load the data to the custom class.

```
private void Update()
{
    nameText.text = myData.data[index].name;
    emailText.text = myData.data[index].email;
}
```
Show the name and email of the data at the UI Text.
